package nz.co.beerbads.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Document(collection= "events")
public class Event {
	
	public Event() {
		// TODO Auto-generated constructor stub
	}
	
	@Id
	private String id;
	private String venueId;
	private String address;
	private String organizerId;
	private String organizerName;
	private String name;
	private String description;
	private String startDate;
	private String endDate;
	private String creationDate;
	private String status;
	private Boolean allowHopIn;
	private int limit;
	private String profilePic;
	private List<Preference> preferences;
}
