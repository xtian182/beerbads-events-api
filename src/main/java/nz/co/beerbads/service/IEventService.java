package nz.co.beerbads.service;

import java.util.List;
import java.util.Optional;
import nz.co.beerbads.dto.EventDto;
import nz.co.beerbads.dto.UserDto;
import nz.co.beerbads.model.Event;

public interface IEventService {
	void addEvent(EventDto e) throws Exception;
	void updateEvent(EventDto e) throws Exception;
	EventDto viewEvent(String eventId) throws Exception;
	List<EventDto> listEvents(String location,String userId) throws Exception;
	List<EventDto> listEventsByUserPref(UserDto userDto,String location) throws Exception;
}
