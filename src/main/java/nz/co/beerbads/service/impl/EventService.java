package nz.co.beerbads.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import nz.co.beerbads.dto.AttendeeDto;
import nz.co.beerbads.dto.EventDto;
import nz.co.beerbads.dto.PreferenceDto;
import nz.co.beerbads.dto.UserDto;
import nz.co.beerbads.dto.VenueDto;
import nz.co.beerbads.model.Attendees;
import nz.co.beerbads.model.Event;
import nz.co.beerbads.model.User;
import nz.co.beerbads.model.Venue;
import nz.co.beerbads.repository.IAttendeesRepository;
import nz.co.beerbads.repository.IEventRepository;
import nz.co.beerbads.repository.IUserRepository;
import nz.co.beerbads.repository.IVenueRepository;
import nz.co.beerbads.service.IEventService;
import nz.co.beerbads.service.mapper.IPreferenceMapper;
import nz.co.beerbads.util.BeerBadsUtil;
import nz.co.beerbads.util.PreferencesAlgorithm;
import nz.co.beerbads.util.SortEventDto;

@Service
public class EventService implements IEventService {
	private static final Logger log = LoggerFactory.getLogger(EventService.class);
	@Autowired
	IEventRepository eventRepo;
	
	@Autowired
	IVenueRepository  venueRepo;
	
	@Autowired
	IAttendeesRepository attendeesRepo;
	
	@Autowired
	PreferencesAlgorithm preferenceAlgorithm;
	
	@Autowired
	SortEventDto sortEvent;
	
	@Autowired
	IUserRepository userRepo;
	
	@Autowired
	IPreferenceMapper ipm;
	
	@Override
	public void addEvent(EventDto e) throws Exception {
		ModelMapper modelMapper = new ModelMapper();
		if(e.getVenueId()!=null && !e.getVenueId().isEmpty()) {
			Optional<Venue> ov = venueRepo.findById(e.getVenueId());
			if(ov.isPresent()) {
				Event event = modelMapper.map(e, Event.class);
				event.setAddress(ov.get().getAddress());
				event.setCreationDate(BeerBadsUtil.getTimestamp());
				eventRepo.save(event);
			} else {
				throw new BeerBadsException(e.getVenueId() +" does not exist");
			}
		} else {
			if(!e.getAddress().isEmpty() && e.getAddress()!=null) {
				Event event = modelMapper.map(e, Event.class);
				event.setCreationDate(BeerBadsUtil.getTimestamp());
				eventRepo.save(event);
			} else {
				throw new BeerBadsException(e.getAddress() +" is invalid");
			}
		}
	}

	@Override
	public void updateEvent(EventDto e) throws Exception {
		Optional<Event> oe = eventRepo.findById(e.getId());
		ModelMapper modelMapper = new ModelMapper();
		if(oe.isPresent()) {
			log.info(e.getId() +" does exist, updating now");
			if(e.getVenueId()!=null && !e.getVenueId().isEmpty()) {
				Optional<Venue> ov = venueRepo.findById(e.getVenueId());
				if(ov.isPresent()) {
					Event event = modelMapper.map(e, Event.class);
					event.setAddress(ov.get().getAddress());
					event.setCreationDate(BeerBadsUtil.getTimestamp());
					eventRepo.save(event);
				} else {
					throw new BeerBadsException(e.getVenueId() +" does not exist");
				}
			} else {
				if(!e.getAddress().isEmpty() && e.getAddress()!=null) {
					Event event = modelMapper.map(e, Event.class);
					event.setCreationDate(BeerBadsUtil.getTimestamp());
					eventRepo.save(event);
				} else {
					throw new BeerBadsException(e.getAddress() +" is invalid");
				}
			}
		} else {
			log.info(e.getId() +" does not exist");
			throw new BeerBadsException(e.getId() +" does not exist");
		}
	}

	@Override
	public EventDto viewEvent(String eventId) throws Exception {		
		Optional<Event> oe = eventRepo.findById(eventId);
		ModelMapper modelMapper = new ModelMapper();
		if(oe.isPresent()) {
			EventDto event = modelMapper.map(oe.get(), EventDto.class);
			Event e = oe.get();
			Venue v = null;
			if(e.getVenueId()!=null && !e.getVenueId().isEmpty()) {
				v = venueRepo.findById(oe.get().getVenueId()).get();
			} else {
				v = new Venue();
			}
			VenueDto venue = modelMapper.map(v, VenueDto.class);
			event.setVenue(venue);
			return event;
		}
		throw new BeerBadsException(eventId +" does not exist");
	}

	@Override
	//@Cacheable(value = "listevents", key="#userId")
	public List<EventDto> listEvents(String location, String userId) throws Exception {
		Optional<List<Event>> oledto = eventRepo.findByAddressLikeOrderByNameAsc(location);
		List<EventDto> listEventDto = new ArrayList<>();
		if(oledto.isPresent()) {
			//log.debug("===initializing cache data===");
			ModelMapper modelMapper = new ModelMapper();
			for(Event e: oledto.get()) {
				EventDto eventDto = modelMapper.map(e, EventDto.class);
				Attendees attendee = attendeesRepo.findByEventIdAndUserId(eventDto.getId(), 
						userId);
				if(attendee != null) {
					attendee.setEventId(null);
					attendee.setUserId(null);
				} else {
					attendee = new Attendees();
					attendee.setStatus("UNCONFIRMED");
				}
				eventDto.setAttendee(modelMapper.map(attendee,AttendeeDto.class));
				if(e.getVenueId()!=null && !e.getVenueId().isEmpty()) {
					Venue venue = venueRepo.findById(e.getVenueId()).get();
					VenueDto venueDto = modelMapper.map(venue, VenueDto.class);
					eventDto.setVenue(venueDto);
				}
				listEventDto.add(eventDto);
			}//for loop Event Model	
		}
		return listEventDto;
	}

	@Override
	public List<EventDto> listEventsByUserPref(UserDto userDto,String location) throws Exception {
		Optional<List<Event>> oledto = eventRepo.findByAddressLikeOrderByNameAsc(location);
		Optional<User> oe = userRepo.findByUserId(userDto.getUserId());
		List<EventDto> listEventDto = new ArrayList<>();
		ModelMapper modelMapper = new ModelMapper();
		if(oledto.isPresent()) {
			List<PreferenceDto> lpdto = ipm.listPrefDto(oe.get().getPreferences());
			for(Event e: oledto.get()) {
				EventDto eventDto = modelMapper.map(e, EventDto.class);
				Attendees attendee = attendeesRepo.findByEventIdAndUserId(eventDto.getId(), 
						userDto.getUserId());
				
				//use preference algorithm
				if(eventDto.getPreferences()!=null) {
					double matchVal = preferenceAlgorithm.getPercentMatch(lpdto,
							eventDto.getPreferences());
					if(matchVal > 0) {
						log.debug("matched value is: "+matchVal+" lpdto "+lpdto.toString());
						eventDto.setMatchedValue(matchVal);
						if(attendee != null) {
							attendee.setEventId(null);
							attendee.setUserId(null);
						} else {
							attendee = new Attendees();
							attendee.setStatus("UNCONFIRMED");
						}
						eventDto.setAttendee(modelMapper.map(attendee,AttendeeDto.class));
						if(e.getVenueId()!=null && !e.getVenueId().isEmpty()) {
							Venue venue = venueRepo.findById(e.getVenueId()).get();
							VenueDto venueDto = modelMapper.map(venue, VenueDto.class);
							eventDto.setVenue(venueDto);
						}
						listEventDto.add(eventDto);
					}
				}
			}//for loop Event Model	
		}//if findByAddressLikeOrderByNameAsc is present
		return listEventDto;
	}
	
}
