package nz.co.beerbads.service.impl;

@SuppressWarnings("serial")
public class BeerBadsException extends Exception  {
	public BeerBadsException(String errorMessage) {
        super(errorMessage);
    }
}
