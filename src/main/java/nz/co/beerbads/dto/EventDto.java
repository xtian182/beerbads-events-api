package nz.co.beerbads.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EventDto {
	public EventDto() {
	}
	@JsonProperty("eventId")
	private String id;
	@JsonProperty("venueId")
	private String venueId;
	@JsonProperty("venueAddress")
	private String address;
	@JsonProperty("organizerId")
	private String organizerId;
	@JsonProperty("organizerName")
	private String organizerName;
	@JsonProperty("eventName")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("startDate")
	private String startDate;
	@JsonProperty("endDate")
	private String endDate;
	@JsonProperty("status")
	private String status;
	@JsonProperty("allowHopIn")
	private Boolean allowHopIn;
	@JsonProperty("limit")
	private int limit;
	private VenueDto venue;
	private AttendeeDto attendee;
	@JsonProperty("preferences")
	private List<PreferenceDto> preferences;
	@JsonProperty("profilePic")
	private String profilePic;
	private double matchedValue;
	private double userPreferencePercentMatch;
}
