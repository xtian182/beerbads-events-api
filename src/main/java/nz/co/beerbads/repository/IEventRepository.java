package nz.co.beerbads.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import nz.co.lps.beerbads.model.Event;

@Repository
public interface IEventRepository extends MongoRepository<Event, String> {
	Optional<Event> findById(String id);
	Optional<List<Event>> findByAddressLikeOrderByNameAsc(String address);
}
