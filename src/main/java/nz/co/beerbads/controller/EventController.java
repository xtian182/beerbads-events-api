package nz.co.beerbads.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import nz.co.beerbads.dto.AttendeeDto;
import nz.co.beerbads.dto.EventDto;
import nz.co.beerbads.dto.PreferenceDto;
import nz.co.beerbads.dto.ResponseDto;
import nz.co.beerbads.dto.UserDto;
import nz.co.beerbads.service.IAttendeesService;
import nz.co.beerbads.service.IEventService;
import nz.co.beerbads.service.impl.BeerBadsException;
import nz.co.beerbads.util.BeerBadsUtil;
import nz.co.beerbads.util.EventsComparator;
import nz.co.beerbads.util.PreferencesAlgorithm;

@Controller
public class EventController {
	private static final Logger log = LoggerFactory.getLogger(EventController.class);
	@Autowired
	private IEventService eventService;
	
	@Autowired
	private IAttendeesService attendeesService;
	
	@Autowired
	PreferencesAlgorithm preferencesAlgorithm;
	
	@Autowired
	EventsComparator eventsComparator;
	
	@GetMapping("venues/events")
	public ResponseEntity<Object> listEvent(@Valid @RequestParam("location") String location
			,@Valid @RequestParam("username") String userId){
		log.debug("=== API listEvent started ===");
		List<EventDto> listEventDto = null;
		try {
			listEventDto = eventService.listEvents(location,userId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("=== API listEvent ended ===");
		return new ResponseEntity<Object>(listEventDto, HttpStatus.OK);
	}
	
	@PostMapping("venues/events")
	public ResponseEntity<Object> addEvent(@Valid @RequestBody EventDto eventsDto){
		log.debug("=== API addEvent started ===");
		try {
			eventService.addEvent(eventsDto);
		}  catch (BeerBadsException e) {
			return new ResponseEntity<Object>(
					new ResponseDto("failed", e.getMessage(), BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(
				new ResponseDto("success", "created event " + eventsDto.getName(), BeerBadsUtil.getTimestamp()),
				HttpStatus.OK);
	}
	
	@PostMapping("venues/events/batch")
	public ResponseEntity<Object> addEventBatch(@Valid @RequestBody List<EventDto> eventsDto){
		log.debug("=== API addEventBatch started ===");
		try {
			for(EventDto e: eventsDto) {
				eventService.addEvent(e);
			}
		} catch (BeerBadsException e) {
			return new ResponseEntity<Object>(
					new ResponseDto("failed", e.getMessage(), BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(
				new ResponseDto("success", "created event for batch", BeerBadsUtil.getTimestamp()),
				HttpStatus.OK);
	}
	
	@PutMapping("venues/events")
	public ResponseEntity<Object> updateEvent(@Valid @RequestBody EventDto eventsDto) throws Exception {
		log.debug("=== API updateEvent started ===");
		try {
			eventService.updateEvent(eventsDto);
		} catch (BeerBadsException e) {
			return new ResponseEntity<Object>(
					new ResponseDto("failed", e.getMessage(), BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(
				new ResponseDto("success", "updated event " + eventsDto.getName(), BeerBadsUtil.getTimestamp()),
				HttpStatus.OK);
	}
	
	@GetMapping("venues/events/{eventId}")
	public ResponseEntity<Object> viewEvent(@Valid @PathVariable("eventId") String eventId) {
		log.debug("=== API viewEvent " + eventId + " started ===");
		EventDto eventsDto = null;
		try {
			eventsDto = eventService.viewEvent(eventId);
		} catch (BeerBadsException e) {
			return new ResponseEntity<Object>(
					new ResponseDto("failed", e.getMessage(), BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(eventsDto, HttpStatus.OK);
	}
	
	@PostMapping("venues/events/attendees/{eventId}")
	public ResponseEntity<Object> joinEvent(@Valid @PathVariable("eventId") String eventId,
			@Valid @RequestParam("userId") String userId,
			@Valid @RequestParam("searchKey") String searchKey){
		log.debug("=== API joinEvent started ===");
		try {
			attendeesService.addAttendee(new AttendeeDto(eventId,userId,"CONFIRMED"),
					searchKey);
		} catch (BeerBadsException e) {
			return new ResponseEntity<Object>(
					new ResponseDto("failed", e.getMessage(), BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(
				new ResponseDto("success", "user " + userId + " successfully join an event", BeerBadsUtil.getTimestamp()),
				HttpStatus.OK);
	}
	
	@GetMapping("venues/events/attendees/{eventId}")
	public ResponseEntity<Object> listAttendees(@Valid @PathVariable("eventId") String eventId){
		log.debug("=== API listAttendees started ===");
		List<AttendeeDto> listAttendee = null;
		try {
			listAttendee = attendeesService.listAttendees(eventId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("=== API listAttendees ended ===");
		return new ResponseEntity<Object>(listAttendee, HttpStatus.OK);
	}
	
	@DeleteMapping("venues/events/attendees/{eventId}")
	public ResponseEntity<Object> deleteAttendee(@Valid @PathVariable("eventId") String eventId,
			@Valid @RequestParam("userId") String userId,
			@Valid @RequestParam("searchKey") String searchKey){
		log.debug("=== API deleteAttendee started ===");
		try {
			attendeesService.deleteAttendee(new AttendeeDto(eventId,userId,""),
					searchKey);
		} catch (BeerBadsException e) {
			return new ResponseEntity<Object>(
					new ResponseDto("failed", e.getMessage(), BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(
				new ResponseDto("success", "user " + userId + " successfully removed from an event", BeerBadsUtil.getTimestamp()),
				HttpStatus.OK);
	}
	
	@GetMapping("venues/events/users")
	public ResponseEntity<Object> listEventByUserPref(@Valid @RequestParam("location") String location,
			@Valid @RequestBody UserDto userDto){
		log.debug("=== API listEvent by Pref started ===");
		List<EventDto> listEventDto = null;
		try {
			listEventDto = eventService.listEventsByUserPref(userDto,location);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(
					new ResponseDto("failed", "service unavailable", BeerBadsUtil.getTimestamp()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("=== API listEvent by Pref ended ===");
		return new ResponseEntity<Object>(listEventDto, HttpStatus.OK);
	}

	@GetMapping("venues/sortevents")
	public ResponseEntity<Object> getEventsByUserPreferences(@Valid @RequestParam("location") String location,
			@Valid @RequestBody UserDto userDto) {

		// TODO: retrieve list of Events per location from the cached data once caching
		// is implemented for best performance.
		// For now temporarily retrieve it from the database
		ResponseEntity<Object> eventListResponse = listEvent(location, userDto.getUserId());

		if (eventListResponse.getBody() != null) {

			ArrayList<PreferenceDto> mainUserPreferences = (ArrayList<PreferenceDto>) userDto.getPreferences();

			ArrayList<EventDto> listOfEvents = (ArrayList<EventDto>) eventListResponse.getBody();

			if (listOfEvents.size() > 0 && mainUserPreferences != null && mainUserPreferences.size() > 0) {

				for (EventDto eventDto : listOfEvents) {
					
					ArrayList<PreferenceDto> secondUserPreferences = (ArrayList<PreferenceDto>) eventDto.getPreferences();
					
					if (secondUserPreferences != null && secondUserPreferences.size() > 0) {
						
						try {		
							eventDto.setUserPreferencePercentMatch(preferencesAlgorithm.getPercentMatch(mainUserPreferences, secondUserPreferences));				
						} catch (Exception e) {
							e.printStackTrace();
							return new ResponseEntity<Object>(
									new ResponseDto("failed", "service unavailable - getEventsByLocationAndPreferences ", BeerBadsUtil.getTimestamp()),
									HttpStatus.INTERNAL_SERVER_ERROR);
						}
						
					}

				}
				
				Collections.sort(listOfEvents, eventsComparator);
				return new ResponseEntity<Object>(listOfEvents, HttpStatus.OK); 

			}

		}

		return eventListResponse;
	}
}
